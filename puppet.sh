#!/bin/sh
echo 128.175.126.33 puppet puppet.anr.udel.edu | sudo tee -a /etc/hosts
curl -L -o ~/install_puppet.py  https://raw.github.com/grahamgilbert/macscripts/master/Puppet-Install/install_puppet.py 
echo $HOSTNAME
hst=$(echo $HOSTNAME | cut -d. -f1).anr.udel.edu
echo $hst
python ~/install_puppet.py --server puppet --certname $hst
#sudo curl -o /etc/puppet/puppet.conf https://bitbucket.org/gkeane/puppetmacscript/raw/master/puppet.conf
sudo puppet resource group puppet ensure=present
sudo puppet resource user puppet ensure=present gid=puppet shell='/sbin/nologin'
sudo curl  -o /Library/LaunchDaemons/com.puppetlabs.puppet.plist  https://bitbucket.org/gkeane/puppetmacscript/raw/master/com.puppetlabs.puppet.plist
sudo chown root:wheel /Library/LaunchDaemons/com.puppetlabs.puppet.plist
sudo chmod 644 /Library/LaunchDaemons/com.puppetlabs.puppet.plist
sudo launchctl load -w /Library/LaunchDaemons/com.puppetlabs.puppet.plist
sudo defaults write /Library/Preferences/com.apple.loginwindow Hide500Users -boolean YES
sudo rm -Rf /etc/simian/
curl -O https://bitbucket.org/gkeane/puppetmacscript/raw/master/simian.dmg
hdiutil attach simian.dmg
sudo /usr/sbin/installer -pkg /Volumes/Simian/simian.pkg -target /
hdiutil eject /Volumes/Simian
puppet agent --test
subject="New Client $hst"
from="puppet@anr.udel.edu"
recipients="gkeane@udel.edu"
mail="subject:$subject\nfrom:$from\nNew puppet client"
echo -e $mail | sendmail "$recipients"
